package tests
import io.gatling.core.Predef.{rampUsers, _}
import io.gatling.http.Predef._


import scala.concurrent.duration.DurationInt
import io.gatling.core.scenario.Simulation

class CreateConsumeTicketsFlow extends Simulation {


  val httpConf = http
    .baseUrl("https://backend-support.staging.tixserve.com/api/")
    .header("Accept", "application/json")
    .header("mob-os-type", "android")
    .header("mob-os-version", "27")
    .header("mob-store-version", "2.11.7.0")
    .header("mob-wallet-public-id", "A4T")



  val scn = scenario("Send multiple tickets to client and consume via doorman")
    .exec(
      http("Get manager access token via manager Login")
        .post("Managers/login")
        .body(ElFileBody("Login.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.userId").is("5149"))
        .check(jsonPath("$.id").saveAs("managerToken"))
    )
    .exec(
      http("Send multiple tickets")
        .post("Vendors/5/createTickets?access_token=${managerToken}")
        .body(ElFileBody("tickets.json")).asJson
        .check(status.is(200))
    )
    .exec(
      http("Get client access token via client login")
        .post("Clients/login")
        .body(ElFileBody("clientLogin.json")).asJson
        .check(status.is(200))
        .check(jsonPath("$.accessToken").saveAs("clientToken"))
    )
    .exec(
      http("Get Tickets list of tickets")
        .get("Clients/5321/ticketList?access_token=${clientToken}")
        .check(status.is(200))
        .check(jsonPath("$[-1:]").saveAs("finalTicketId"))
    )
    .exec(
      http("Doorman gets event info")
        .get("Events/TE-RSHWTL/doorman?access_token=${managerToken}")
        .check(status.is(200))
        .check(jsonPath("$.eventId").is("115"))
    )
    .exec(
      http("Doorman checks event password")
        .post("Events/doorman/check?access_token=${managerToken}")
        .body(ElFileBody("eventPassword.json")).asJson
        .check(status.is(200))
    )
    .exec(
      http("Doorman scans ticket and its consumed")
        .put("Tickets/${finalTicketId}/selfConsume?access_token=${clientToken}")
        .check(status.is(200))
        //.check(jsonPath("$.ticketId").is("${finalTicketId}"))
        .check(jsonPath("$.ticketId").is("${finalTicketId}"))
    )


  setUp(

    scn.inject(
      nothingFor(2.seconds),
      rampUsers(10) during (5.seconds))
    ).protocols(httpConf)
    .assertions(
      global.responseTime.max.lt(1000),
      global.successfulRequests.percent.gt(95)
    )

}
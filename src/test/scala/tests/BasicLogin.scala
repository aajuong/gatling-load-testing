package tests
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration.DurationInt
import io.gatling.core.scenario.Simulation


class BasicLogin extends Simulation {

  /*** Before ***/
  before {
    println("Running basic log in test")
  }

    val httpConf = http
      .baseUrl("https://backend-support.staging.tixserve.com/api/")
      .header("Accept", "application/json")

    val scn = scenario("Log in")
      .exec(
        http("Login endpoint")
        .post("Managers/login")
        .body(ElFileBody("Login.json")).asJson
        .check(status.is(200))
    )

    setUp(
      scn.inject(
        nothingFor(5.seconds),
        atOnceUsers(1),
        rampUsers(1) during (10.seconds)))
      .protocols(httpConf)
      .assertions(
        global.responseTime.max.lt(800),
        global.successfulRequests.percent.gt(95)
      )
  
  // After
  after {
    println("Basic Login Test complete")
  }
  }
